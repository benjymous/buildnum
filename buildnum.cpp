#include <iostream>
#include <fstream>

int main (int argc, char* argv[])
{
  std::fstream file;
  file.open ("buildnum", std::ios::in);
  int number = 0;
  file >> number;
  file.close();

  number++;
  std::cout << "buildnum is now " << number << std::endl;

  file.open ("buildnum", std::ios::out | std::ios::trunc);
  file << number;
  file.close();

  file.open ("buildnum.h", std::ios::out | std::ios::trunc);
  file << "#define VERSION_BUILD " << number << std::endl;
  file.close();


}
